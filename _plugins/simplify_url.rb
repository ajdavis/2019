require 'liquid'
require 'uri'

module SimplifyURL
  def simplify_url(url)
    url = URI.parse(url)
    return url.host + (url.path != '/' ? url.path : '')
  end
end

Liquid::Template.register_filter(SimplifyURL)
