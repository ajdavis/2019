---
name: Bit by Bit
tier: community
site_url: http://bitbybit.dalton.org
logo: bit-by-bit.jpg
twitter: bit_by_bit_nyc
---
Bit by Bit is a free annual conference organized by high school girls for high school girls at the
Dalton School. The conference is dedicated to breaking the gender barrier in tech and gathering
together empowering women to create a strong community for the female coders of the future. If you
are interested in getting involved with the event, reach out at
[bitbybitnyc@gmail.com](mailto:bitbybitnyc@gmail.com)!
