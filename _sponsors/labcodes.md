---
name: Labcodes
tier: silver
site_url: https://labcodes.com.br/?utm_source=pygotham&utm_medium=social_media&utm_campaign=pygotham
logo: labcodes.png
twitter: labcodes
---
Labcodes is a software studio based in Brazil that designs, implements and scales digital products.
We deliver great experiences and build web applications that fit customers’ needs using Python,
Django and React. Our projects are centered in creating unique solutions that bring value to their
users, and therefore to our clients.

We’ve been partnering with US clients for almost 5 years helping a big variety of companies, from
1-person startups and YC startups to well stablished companies. Our team is talented and recognized
worldwide, giving talks and mentoring people.
