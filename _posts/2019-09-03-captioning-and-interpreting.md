---
title: Captioning and ASL Interpreters
date: 2019-09-03 00:00:00 -0400
excerpt_separator: <!--more-->
---
This year, PyGotham will provide both live captioning (CART) and American Sign
Language (ASL) interpretation. This ensures our conference is accessible to Deaf
and hard of hearing attendees, and brings a bunch of additional benefits:
<!--more--> Captioning talks makes them easier to follow for anyone who speaks
English as a second language, and it can help if you didn't understand something
the speaker said, or if your attention lapsed for a few seconds. After the
conference, we can use the captioners' live transcripts as subtitles when we
upload videos.

All talks in all three tracks, plus keynotes and lightning talks, will be live
captioned and interpreted. If you would like us direct you to interpreters
on-site, please check the "ASL" box [when you register]({% link
registration/index.md %}). If you registered already and weren't presented with
additional questions or didn't check the box, you can edit your response through
the "manage your order" link in your confirmation email. If you have any
questions about PyGotham's accessibility, please email
[organizers@pygotham.org](mailto:organizers@pygotham.org).

Thanks to our generous [sponsors]({% link sponsors/index.md %}) and attendees
who made this possible. If you'd like to support these efforts, reach out to
[sponsors@pygotham.org](mailto:sponsors@pygotham.org). We haven't provided
captioning and ASL interpretation to this degree before and we expect to make
mistakes, but we're excited to make PyGotham 2019 the most accessible ever.
