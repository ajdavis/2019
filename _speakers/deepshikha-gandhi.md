---
name: Deepshikha Gandhi
talks:
- "How to Effectively Reduce AI Pipeline Runtime"
---

Deepshikha Gandhi is the [Hux by Deloitte
Digital](https://www.deloittedigital.com/us/en/offerings/customer-led-marketing/advertising--marketing-and-commerce/hux.html?id=us%253A2sm%253A3tw%253A4Qualtrics%2520X4%253A%253A6oth%253A20190305190000%253A&linkId=64352730)
Data DevOps lead on the Technical Operations team. Her focus is to automate,
build, scale and maintain the Hux AI infrastructure. She is also dedicated
to evolving the organization’s data processing and machine learning
capabilities, making the process more efficient, repeatable and scalable.

Deepshikha joined Hux by Deloitte Digital, a part of Deloitte Consulting
LLP, in 2018 through the Magnetic acquisition. She holds a Master of Science
in Electrical and Computer Engineering from Georgia Institute of Technology.
In her spare time, Deepshikha enjoys traveling with her husband. 
