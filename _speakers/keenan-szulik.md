---
name: Keenan Szulik
talks:
- "Open source sustainability: what does this mean for Python?"
---
I work as a Product Manager at Tidelift, where I'm leading our efforts
partnering with open source maintainers to help financially support their
projects. I've built partnerships with dozens of individual Python
maintainers, as well as prominent Python foundations such as the Python
Software Foundation and NumFOCUS. Before this, I worked as a data scientist,
primarily in Python :)
