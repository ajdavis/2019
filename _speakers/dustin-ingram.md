---
name: Dustin Ingram
talks:
- "Static Typing in Python"
---
Dustin is a Developer Advocate at Google, focused on supporting the Python
community on [Google Cloud](https://cloud.google.com/python). He's also a
member of the [Python Packaging Authority](https://github.com/pypa),
maintainer of the [Python Package Index](https://pypi.org/), and organizer
for the [PyTexas](https://pytexas.org/) conference.
